const express = require("express");
const app = express();
const PORT = 3000;

// Baseurl: http//:localhost:3000
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
    //request, response
    res.send("Hello World!");
});

/**
 * This is an arrow function to add two numbers together.
 * @param {Number} a is the first number
 * @param {Number} b is the second number/parameter
 * @return {Number} This is the sum of a and b params.
 */

const add = (a, b) => {
    const sum = a + b;
    return sum;
};

app.use(express.json()); // This is middleware
// Endpoint : http://localhost:3000/add
app.post('/add', (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const sum = add(a, b);
    res.send(sum.toString());
});

// This starts the server
app.listen(PORT, () => console.log(
    `Server listening ar http://localhost:${PORT}`
));

console.log("Express application starting...");